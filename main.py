from bs4 import BeautifulSoup
import requests
import json

from src.Airport import *
from src.RailwayStation import *
from src.logger import *


class Main:
	__logger = Logger()

	def __init__(self):
		urls = [
			'Внуково_(аэропорт)',
			'Казанский_вокзал',
			'Домодедово_(аэропорт)',
			'Белорусский_вокзал',
		]
		self.parse(urls)
		print()
		self.show()

	def parse(self, urls):
		data = []

		for url in urls:
			res = requests.get(f'https://ru.wikipedia.org/wiki/{url}')

			if res.status_code != 200:
				return

			contentTree = BeautifulSoup(res.text, 'html.parser')
			table = contentTree.find('table', class_='infobox')

			if table:
				for el in table.find('tr').findAll():
					if el.name == 'a':
						el.decompose()

				obj = {
					'name': table.find('tr').text.strip(),
					'type': 'Airport' if table.find('tr').text.strip().startswith('Аэропорт') else 'Railway',
					'info': [],
				}

				for tr in table.findAll('tr'):
					th = tr.findChildren('th')
					td = tr.findChildren('td')

					if th and td:
						obj['info'].append({
							'key': th[0].getText().strip(),
							'val': td[0].getText().strip(),
						})
					else:
						if len(td) == 2:
							obj['info'].append({
								'key': td[0].getText().strip(),
								'val': td[1].getText().strip(),
							})

				data.append(obj)
				self.__logger.cmd('Добавление: ' + table.find('tr').text.strip())

		self.save(data)

	def save(self, data, file=None):
		pass
		with open(file or './save.json', 'w', encoding='utf-8') as f:
			f.write(json.dumps(data))
			self.__logger.cmd('Данные сохранены', 7)

	def show(self, file=None):
		with open(file or './save.json', 'r', encoding='utf-8') as data:
			self.__logger.cmd('Данные загружены', 2)
			for obj in json.load(data) or {}:
				self.__logger.cmd(obj['name'], 4)

				for item in obj['info']:
					print(f'{item["key"]} — {item["val"]}')


if __name__ == '__main__':
	Main()
